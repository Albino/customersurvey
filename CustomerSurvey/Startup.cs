﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CustomerSurvey.Startup))]
namespace CustomerSurvey
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
