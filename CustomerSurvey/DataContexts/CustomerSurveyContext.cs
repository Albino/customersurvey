﻿using CustomerSurvey.Entities.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CustomerSurvey.DataContexts
{
    public class CustomerSurveyContext : DbContext
    {
        public CustomerSurveyContext():
            base("CustomerSurveyDbConnectionString")
        {
        }
        // Survey table
        public DbSet<Survey> Surveys { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}