﻿using CustomerSurvey.Entities.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace CustomerSurvey.DataContexts
{
    public class CustomerSurveyContextInitializer : DropCreateDatabaseIfModelChanges<CustomerSurveyContext>
    {
        protected override void Seed(CustomerSurveyContext context)
        {
            var surveys = new List<Survey>
            {
                new Survey {DescriptiveWords=new List<DescriptiveWord> { DescriptiveWord.GoodValue,DescriptiveWord.HighQuality} ,Comments="Giberish",EntryDate=DateTime.Now.AddDays(-3),HowSatisfied=Satisfaction.SomewhatSatisfied},
            };

            surveys.ForEach(s => context.Surveys.Add(s));
            context.SaveChanges();
        }
    }
}