﻿using System.Collections.Generic;
using CustomerSurvey.Entities.Models;
using Chart.Mvc.ComplexChart;
using System.Linq;

namespace CustomerSurvey.Models
{
    public class StatisticsViewModel
    {
        private List<Survey> surveys;

        public StatisticsViewModel(List<Survey> surveys)
        {
            this.surveys = surveys;
            ComputeHistograms();
        }

        public List<double> Satisfaction { get; private set; }
        public List<double> LikelyToBuy { get; private set; }
        public List<double> Responsive { get; private set; }

        public void ComputeHistograms()
        {
            List<double> likelyToBuy = new List<double> {0,0,0,0,0};
            List<double> satisfaction = new List<double> {0,0,0,0,0};
            List<double> responsive = new List<double> {0,0,0,0,0};

            foreach(Survey survey in this.surveys)
            {
                likelyToBuy[(int)survey.LikelyToPurchaseAgain]++;
                satisfaction[(int)survey.HowSatisfied]++;
                responsive[(int)survey.HowResponsive]++;
            }

            LikelyToBuy = likelyToBuy;
            Satisfaction = satisfaction;
            Responsive = responsive;
        }
    }
}