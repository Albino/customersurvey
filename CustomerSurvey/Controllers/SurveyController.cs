﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using CustomerSurvey.Entities.Models;
using CustomerSurvey.DataContexts;
using CustomerSurvey.Models;

namespace CustomerSurvey.Controllers
{
    [Authorize(Users = "admin@admin.com" )]
    public class SurveyController : Controller
    {
        private CustomerSurveyContext db = new CustomerSurveyContext();

        // GET: Survey
        public async Task<ActionResult> Index()
        {
            return View(await db.Surveys.ToListAsync());
        }

        // Get : Thank you for the feedback page
        [AllowAnonymous]
        public ActionResult Thanks()
        {
            return PartialView();
        }

        // GET: Survey/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = await db.Surveys.FindAsync(id);
            if (survey == null)
            {
                return HttpNotFound();
            }
            return View(survey);
        }

        // GET: Survey/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            var newSurvey = new Survey();
            return PartialView(newSurvey);
        }

        // POST: Survey/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,EntryDate,HowSatisfied,HowResponsive,HowLongHaveBeenCustomer,LikelyToPurchaseAgain,LikelyToRecommend,Comments")] Survey survey)
        {
            if (ModelState.IsValid)
            {
                survey.EntryDate = DateTime.Now;
                db.Surveys.Add(survey);
                await db.SaveChangesAsync();
                return RedirectToAction("Thanks");
            }
            return PartialView(survey);
        }

        // GET: Survey/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Survey survey = await db.Surveys.FindAsync(id);
            if (survey == null)
            {
                return HttpNotFound();
            }
            return View(survey);
        }

        // POST: Survey/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Survey survey = await db.Surveys.FindAsync(id);
            db.Surveys.Remove(survey);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Statistics()
        {
            var surveys = await db.Surveys.ToListAsync();
            var statistics = new StatisticsViewModel(surveys);
            return View(statistics);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
