﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CustomerSurvey.Entities.Models
{
    public class Survey
    {
        public int ID { get; set; }
        [DisplayName("Survey registration date")]
        public DateTime EntryDate { get; set; }
        [DisplayName("Overall, how satisfied or dissatisfied are you with our company?")]
        public Satisfaction HowSatisfied { get; set; }
        [DisplayName("Which of the following workds would you use to describe our products? Select all that apply.")]
        public ICollection<DescriptiveWord> DescriptiveWords { get; set;}
        [DisplayName("How responsive have we been to your question of concerns about our products?")]
        public Responsiveness HowResponsive { get; set; }
        [DisplayName("How long have you been a customer or our company?")]
        public TimeAsCustomer HowLongHaveBeenCustomer { get; set; }
        [DisplayName("How likely are you to purchase any of our products again?")]
        public LilelyToBuyAgain LikelyToPurchaseAgain { get; set; }
        [DisplayName("How likely is it that you would recommend this company to a friend or colleague?")]
        public int LikelyToRecommend { get; set; }
        [DisplayName("Do you have any other comments, questions, or concerns?")]
        public string Comments { get; set; }
    }

    #region Enums
    public enum Satisfaction
    {
        [Display(Name = "Very satisfied")]
        VerySatisfied,
        [Display(Name = "Somewhat satisfied")]
        SomewhatSatisfied,
        [Display(Name = "Neither satisfied nor dissatisfied")]
        NeitherNor,
        [Display(Name = "Somewhat dissatisfied")]
        SomewhatDissastified,
        [Display(Name = "Very dissatisfied")]
        VeryDissastisfied
    }

    public enum DescriptiveWord
    {
        Reliable,
        [Display(Name = "High quality")]
        HighQuality,
        Useful,
        Unique,
        [Display(Name = "Good value for the money")]
        GoodValue,
        Overpriced,
        Impractical,
        Ineffective,
        [Display(Name = "Poor quality")]
        Poorquality,
        Inreliable
    }

    public enum LilelyToBuyAgain
    {
        [Display(Name = "Extremely likely")]
        Extremely,
        [Display(Name = "Very likely")]
        Very,
        [Display(Name = "Somewhat likely")]
        Somewhat,
        [Display(Name = "Not so likely")]
        Notso,
        [Display(Name = "Not at all likely")]
        Notatall
    }

    public enum TimeAsCustomer
    {
        [Display(Name = "This is my first purchase")]
        FirstPurchase,
        [Display(Name = "Less than six months")]
        LessThanSixMonths,
        [Display(Name = "Six months to a year")]
        SixMonthsToaYear,
        [Display(Name = "1 - 2 years")]
        OnetoTwoYears,
        [Display(Name = "3 or more years")]
        ThreeorMore,
    }

    public enum Responsiveness
    {
        [Display(Name = "Extremely responsive")]
        Extremely,
        [Display(Name = "Very responsive")]
        Very,
        [Display(Name = "Somewhat responsive")]
        Somewhat,
        [Display(Name = "Not so responsive")]
        NotSo,
        [Display(Name = "Not at all")]
        NotAtAll,
        [Display(Name = "Not applicable")]
        NotApplicable
    }
    #endregion

}
